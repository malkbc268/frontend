FROM node:14 AS build
ARG SERVER_URL=localhost
ENV SERVER_URL ${SERVER_URL}
RUN apt update -y && apt install -y gettext-base
WORKDIR /app
ADD package.json .
ADD babel.config.js .
ADD src/ /app/src/
RUN /bin/bash -c "sed -i s/'localhost'/'${SERVER_URL}'/gi /app/src/main.js" && \
    npm install && npm run build

FROM nginx:1.19 AS front_server
ARG SERVER_URL=localhost
ENV SERVER_URL ${SERVER_URL}
RUN apt update -y && apt install -y gettext-base
COPY --from=build /app/dist /var/www/exhibition
ADD public/favicon.ico /var/www/exhibition/
ADD etc/nginx.conf /etc/nginx/nginx.conf
ADD etc/exhibition.conf /etc/nginx/conf.d/exhibition.conf
RUN /bin/bash -c "sed -i s/'localhost'/'${SERVER_URL}'/ /etc/nginx/conf.d/exhibition.conf"
